# Source: http://42.tut.by/489053
#
# Task: Я загадал целое десятизначное число, все цифры которого – разные.
# Число, составленное из первых n цифр загаданного числа (без перестановок),
# делится на n без остатка. Это актуально для любого n от 1 до 10. Какое число я загадал?
#
# Answer: 3816547290

import unittest
import math

def solve():
    result = -1
    symbols = list(range(0, 10, 1))
    for i in filterSequence(0, symbols):
        result = step(i, filterSequence(i, symbols))
        if result != -1:
            break
    return result

def step(number, symbols):
    numberLen = math.ceil(math.log10(number + 0.5))
    print(str(number) + " " + str(numberLen) + " " + str(list(symbols)))
    if number % numberLen == 0:
        if len(symbols) > 0:
            for i in symbols:
                result = step(number * 10 + i, filterSequence(i, symbols))
                if result != -1:
                    return result
            return -1
        else:
            return number
    else:
        return -1

def filterSequence(x, sequence):
    return [i for i in sequence if i != x]

class TestCase(unittest.TestCase):
    def test_algorithm(self):
        self.assertEqual(3816547290, solve())
        None

if __name__ == '__main__':
    unittest.main()
